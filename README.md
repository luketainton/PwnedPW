# PwnedPW
Check if your password has been leaked

Thanks to Troy Hunt at [haveibeenpwned.com](https://haveibeenpwned.com) for providing the API powering this tool.

## MOVED TO GITLAB
This repository has moved to [GitLab](https://gitlab.com/luketainton/PwnedPW). All future commits will be mirrored back to GitHub, but changes will only be accepted on the GitLab repository.